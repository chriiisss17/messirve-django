from rest_framework import serializers
from .models import CustomUser, Contacto


class userSerializer(serializers.ModelSerializer):

    password1 = serializers.CharField(style={'input_type' : 'password'}, write_only=True)
    password2 = serializers.CharField(style={'input_type' : 'password'}, write_only=True)

    class Meta:
        model = CustomUser
        fields = ('username','first_name','last_name',
        'email','phone','password1','password2')
        extra_kwargs = {
            'password' : {'write_only': True}
        }
    def save(self):
        user = CustomUser(
            username = self.validated_data['username']
        )
        password1 = self.validated_data['password1']
        password2 = self.validated_data['password2']

        if (password1 != password2):
            raise serializers.ValidationError({'password': 'Las contraseñas no concuerdan'})
        user.set_password(password1)
        user.save()
        return user

    

    
class contactoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contacto
        fields = ('id','rut','comentario','imagen')
