from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import fields
from .models import CustomUser,Contacto
from django.contrib.auth import authenticate

class FormUser(UserCreationForm):

    password1 = forms.CharField(label = 'Contraseña', widget = forms.PasswordInput(
        attrs = {
            'class': 'form-control',
            'placeholder': 'Ingrese su contraseña',
            'id': 'password',
            'name': 'password',
            'required': 'required',
        }
    ))

    password2 = forms.CharField(label = 'Confirme su contraseña', widget = forms.PasswordInput(
        attrs = {
            'class': 'form-control',
            'placeholder': 'Ingrese su contraseña',
            'id': 'password2',
            'required': 'required',
        }
    ))


    class Meta:
        model = CustomUser
        fields = ['username','first_name','last_name','email','phone']
        widgets = {
            'username': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Ingrese su rut',
                    'id': 'username',
                    'required': 'required',
                }
            ),
            'first_name': forms.TextInput( 
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Ingrese su nombre',
                    'id': 'first_name',
                    'required': 'required',
                }
            ),
            'last_name': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Ingrese su apellido',
                    'id': 'last_name',
                    'required': 'required',
                }
            ),
            'email': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Ingrese su correo',
                    'id': 'email',
                    'required': 'required',
                }
            ),
            'phone': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Ingrese su teléfono',
                    'id': 'phone',
                    'required': 'required',
                }
            ),

        }


class UserLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(
        attrs = {
            'class': 'form-control',
            'placeholder': 'Ingrese su rut',
            'id': 'username',
            'required': 'required'
        }
    ))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs = {
            'class': 'form-control',
            'placeholder': 'Ingrese su contraseña',
            'id': 'password',
            'required': 'required'
        }
    ))
    

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password', None)


        if username and password:
            user = authenticate(username=username, password=password)
            print(user)
            if not user:
                raise forms.ValidationError('Usuario y/o Contraseña incorrecta')
        return super(UserLoginForm, self).clean(*args, **kwargs)


