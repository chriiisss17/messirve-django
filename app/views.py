from django.http import HttpResponse, JsonResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render, redirect,get_object_or_404
from django.views.decorators.csrf import csrf_protect
from .models import Contacto,CustomUser
from datetime import datetime
from django.db import IntegrityError
from django.db.models import F
from app.forms import FormUser,UserLoginForm
from django.contrib.auth import authenticate,login,logout as do_logout
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from .serializer import userSerializer, contactoSerializer
from django.contrib import messages
from allauth.account.adapter import DefaultAccountAdapter
import requests

def home(request):
    return render(request,'app/home.html')

def galeria(request):
    return render(request, 'app/galeria.html')

def contacto(request):
    current_user = request.user
    if not current_user.is_authenticated:
        return redirect('home')
    else:
        print(current_user.first_name)
    return render(request, 'app/contacto.html')


@csrf_protect
def save_contact_info(request):
    current_user = request.user
    if not current_user.is_authenticated:
        return redirect('home')
    else:
        #url = "http://127.0.0.1:8000/api/contact/"
        #requests.post(url, data={'rut': current_user.username, 'comentario': request.POST['comentario'], 'imagen': request.FILES['imagen']})
        contacto=Contacto(
            rut=current_user.username,
            comentario=request.POST['comentario'],
            imagen=request.FILES['imagen']
        )
        contacto.save()
    return render(request,"app/contacto.html")

def register(request):
    form=FormUser(request.POST)
    if request.method=='POST':
        if form.is_valid():
            data = form.cleaned_data
            url = "http://127.0.0.1:8000/api/users/"
            requests.post(url, data=data)
    context = {'form' : form}
    return render(request, "app/registro.html", context)

def login_user(request):

    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password', None)
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect(home)
    context = {
        'form' : form}
    return render(request, 'app/login.html', context)

def logout(request):
    do_logout(request)
    return redirect('account_login')

def list_contact(request):
    response = requests.get('http://127.0.0.1:8000/api/users')
    usuarios = response.json()
    response2 =  requests.get('http://127.0.0.1:8000/api/contacts')
    contactos = response2.json()
    return render(request,'app/listar-contacto.html', {'usuarios': usuarios,
    'contactos': contactos})

def delete_contact(request,id):
    contacto=get_object_or_404(Contacto,id=id)
    contacto.delete()
    return redirect("listar-contacto")


def delete_user(request,username):
   # url = "http://127.0.0.1:8000/api/users/{username}"
   # requests.delete(url)
   # if status.is_success(200):
   #     message = 'Usuario eliminado'
    user = get_object_or_404(CustomUser, username=username)
    user.delete()
    return redirect("listar-contacto")

@api_view(['GET', 'POST'])
def api_users(request):

    if request.method == 'GET':
        users = CustomUser.objects.all()
        serializer = userSerializer(users, many=True)
        return Response(serializer.data)
    
    elif request.method == 'POST':
        serializer = userSerializer(data=request.data)
        data={}
        if(serializer.is_valid()):
            user = serializer.save()
            data['response'] = "Cuenta creada correctamente"
            data['username'] = user.username
        else:
            data = serializer.errors
        return Response(data)

@api_view(['GET', 'DELETE'])
def api_user_by_id(request,username):
    try:
        user = CustomUser.objects.get(username=username)
    except CustomUser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == 'GET':
        serializer = userSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'DELETE':
        user.delete()
        return Response(status, status=status.HTTP_200_OK)

@api_view(['POST', 'GET'])
def api_contact(request):
    if request.method == 'POST':
        serializer = contactoSerializer(data=request.data)
        if (serializer.is_valid()):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def api_all_contacts(request):
    contactos = Contacto.objects.all()
    serializer = contactoSerializer(contactos, many=True)
    return Response(serializer.data)


@api_view(['DELETE'])
def api_delete_contact(request,id):
    try:
        contacto = Contacto.objects.get(id=id)
    except Contacto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    contacto.delete()
    return Response(status=status.HTTP_200_OK)



class AccountAdapter(DefaultAccountAdapter):
  def get_login_redirect_url(self, request):
      return '/'