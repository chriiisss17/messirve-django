from django.contrib import admin
from .models import CustomUser,Contacto

# Register your models here.
admin.site.register(CustomUser)
admin.site.register(Contacto)