from django.db import models
from django.db.models.deletion import PROTECT
from django.contrib.auth.models import AbstractUser,BaseUserManager

class Contacto(models.Model):
    id = models.IntegerField(primary_key=True)
    rut = models.CharField(max_length=13)
    comentario = models.CharField(max_length=450, blank=True, null=True)
    imagen = models.ImageField(upload_to='adjunt',blank=True, null=True)

    def __str__(self):
        return self.rut

class CustomUser(AbstractUser):
    phone=models.CharField(max_length=9)
    
    def __str__(self):
        return self.username
