from django.contrib import admin
from messirve.models import Contacto, ContactoInfo

admin.site.register(Contacto)
admin.site.register(ContactoInfo)