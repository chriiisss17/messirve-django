from django.contrib import admin
from django.urls import path, include
from app.views import home,register,galeria,contacto,save_contact_info,login_user,logout,list_contact, delete_contact, delete_user, api_users, api_user_by_id, api_contact, api_delete_contact, api_all_contacts
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers

urlpatterns = [
    path('',home),
    path('admin/', admin.site.urls),
    path('home/', home,name="home"),
    path('galeria/',galeria),
    path('contacto/',contacto),
    path('save_contact_info',save_contact_info),
    path('registro',register,name="registro"),
    path('login/',login_user,name='login'),
    path('logout',logout,name='logout'),
    path('listar-contacto/',list_contact,name='listar-contacto'),
    path('eliminar-contacto/<id>/',delete_contact,name='eliminar-contacto'),
    path('eliminar-usuario/<username>/',delete_user,name='eliminar-usuario'),
    path('api/users/',api_users, name = "api-usuarios"),
    path('api/user/<username>',api_user_by_id, name = "api-usuarios-id"),
    path('api/contact/',api_contact,name = "api-contactos"),
    path('api/contact/delete/<id>',api_delete_contact,name = "eliminar"),
    path('api/contacts',api_all_contacts, name = "all-contacts"),
    path('accounts/', include('allauth.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)