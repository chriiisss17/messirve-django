# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Contacto(models.Model):
    rut = models.CharField(primary_key=True, max_length=10)
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    telefono = models.IntegerField()
    correo = models.CharField(max_length=50)

    def __str__(self):
        return 'RUT:{0} | NOMBRE:{1}'.format(self.rut,self.nombres)
    

    usuarios = models.Manager() 

    class Meta:
        managed = False
        db_table = 'contacto'



class ContactoInfo(models.Model):
    rut_contacto = models.CharField(primary_key=True, max_length=10)
    comentario = models.CharField(max_length=100)
    adjunto = models.ImageField(upload_to='adjunt',blank=True, null=True)

    def __str__(self):
        return 'RUT:{0} | COMENTARIO:{1} | ADJUNTO:{2}'.format(self.rut_contacto,self.comentario,self.adjunto)

    informacion = models.Manager() 

    class Meta:
        managed = False
        db_table = 'contacto_info'